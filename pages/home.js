import React, {Component} from 'react';
import {StyleSheet, ScrollView, Alert, StatusBar} from 'react-native'
import {
  View,
  Text,
  NavigationBar,
  Icon,
  Title,
  Image,
  Button,
  Caption,
  TextInput,
  DropDownMenu
} from '@shoutem/ui';

import {
  createTransition,
  Fade,
  FlipX,
  FlipY,
  SlideLeft,
  SlideRight,
  SlideUp,
  SlideDown
} from 'react-native-transition';

export default class home extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    const resizeMode = 'cover'
    return (
      <View styleName="fill-parent vertical h-center" style={styles.wrapper}>
      <StatusBar
            backgroundColor="#0c3959"
            barStyle="light-content"
        />
        <Image styleName="fill-parent" style={{}} source={require('./assets/bg.png')}/>
        <ScrollView
          style={{
          flex: 1
        }}
          stickyHeaderIndices={[0]}
          contentContainerStyle={styles.contentWrapper}>
          <View
            Style={{
            width: '100%',
            flex: 1,
            height: 60,
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 19,
            backgroundColor: '#0D3B5B',
            borderBottomColor: 'rgba(0,0,0,0.2)',
            borderBottomWidth: 6
          }}
            styleName="fill-parent horizontal h-center v-center">
            <Image style={styles.logo} source={require('./assets/icon.png')} />
            <Icon
              name="turn-off"
              style={{
              marginRight: 12,
              color: '#ffffff',
              fontSize: 25
            }}
              onPress={() => {
                Alert.alert(
                  'Exit App',
                  'Do you want to exit?', [{
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel'
                  }, {
                    text: 'OK',
                    onPress: () => BackHandler.exitApp()
                  }, ], {
                    cancelable: false
                  }
                )
              }}/>
          </View>

          <View style={styles.content} styleName="vertical">
            <Text style={styles.invisible}>
              ___________________________________________________________________________________________________________________
            </Text>
            <Text style={styles.promptText}>
              Select Mode
            </Text>

            <Button
              styleName="confirmation full-width"
              style={styles.live}
              onPress={() => {
              this
                .props
                .nav('hooklive', SlideLeft);
            }}>
              <Text style={styles.whiteText}>Live</Text>
            </Button>
            <Button
              styleName="confirmation full-width"
              style={styles.electronic}
              onPress={() => {
              this
                .props
                .nav('electronic', SlideLeft);
            }}>
              <Text style={styles.whiteText}>Electronic</Text>
            </Button>

          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = {
  wrapper: {
    // justifyContent: 'center', alignItems: 'center',
    backgroundColor: '#34495e',
    //flex: 1,
    position: 'absolute'
  },
  contentWrapper: {
    // width: '100%'
  },
  splashText: {
    color: '#ffffff',
    padding: 10,
    marginBottom: 10
  },
  promptText: {
    color: '#ffffff',
    marginBottom: 15
  },
  singleRow: {
    // justifyContent: 'center' flex: 1,
    //

    padding: 15
    // alignItems: 'center', width: '70%'
  },
  subtitle: {
    fontSize: 12,
    fontWeight: '200',
    color: '#fefefe',
    padding: 12
  },
  title: {
    fontSize: 40,
    color: '#ffffff'
  },
  splashSubTitle: {
    paddingBottom: 20
  },
  live: {
    marginBottom: 20,
    backgroundColor: '#5FA9DD'
  },
  electronic: {
    marginBottom: 20,
    backgroundColor: '#3AB451'
  },
  header: {
    width: '100%',
    flex: 1,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 19,
    backgroundColor: '#0D3B5B',
    borderBottomColor: 'rgba(0,0,0,0.1)',
    borderBottomWidth: 4
  },
  logo: {
    height: 30,
    width: 100,
    flex: 1,
    padding: 10,
    marginLeft: 12
  },
  headerText: {
    color: "#ffffff",
    flex: 3
  },
  whiteText: {
    color: "#ffffff",
    // flex: 9
  },
  invisible: {
    color: "rgba(0,0,0,0)",
    // flex: 9
  },
  content: {
    // height: 1200 flex: 9 flex: 9,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 12,
    paddingBottom: 100,
    // width: '100%'

  }
};
