import React, {Component} from 'react';
import {StyleSheet, Alert, BackHandler, AsyncStorage, ToastAndroid} from 'react-native'
import {
  View,
  Text,
  NavigationBar,
  Icon,
  Title,
  Image,
  Button,
  Caption
} from '@shoutem/ui';

import {
  createTransition,
  Fade,
  FlipX,
  FlipY,
  SlideLeft,
  SlideRight,
  SlideUp,
  SlideDown
} from 'react-native-transition';

import Login from './login';
import Register from './register';
import Main from './main';
import Language from './language';
import Home from './home';
import Electronic from './electronic';
import Live from './live';
import LiveIn from './liveIn';
import Hooklive from './hooklive';
import Call from './call';

Transition = createTransition();

import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyBMUGgaG0htfhogDTV7cb8pSvCJGwiNPto",
  authDomain: "olango-2b6a8.firebaseapp.com",
  databaseURL: "https://olango-2b6a8.firebaseio.com",
  projectId: "olango-2b6a8",
  storageBucket: "olango-2b6a8.appspot.com",
  messagingSenderId: "446115547965"
};


var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

function isEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email.toLowerCase());
}
var fireScratch = firebase.initializeApp(config);
var callInfoPin = {};
PaymentStatus = false;
userPaymentMail = '';
export default class intro extends Component {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this
    .handleBackButtonClick
    .bind(this);
    this.gotoPage = this.gotoPage;
    this.state = {
      previousPage: 'intro',
      currentPage: 'intro',
      loggedIn: false,
      email: ''
    };
    // AsyncStorage
    AsyncStorage.getItem('UIID001', (err, user) => {
      // console.warn(user);
      if (user !== null && isEmail(user)) {
        userPaymentMail = user;
        var userMail = Base64.encode(user);
        var payz = fireScratch
        .database()
        .ref('/payz/' + userMail);
        payz.on('value', (single_user) => {
          var currUser = single_user.val();
          // console.warn(currUser);
          if (currUser != null) {
            PaymentStatus = true
          }
          // console.warn(single_user.val())
          // PaymentStatus = true
        });
        ToastAndroid.showWithGravity('Welcome!', ToastAndroid.LONG, ToastAndroid.CENTER);
        this.setState({ email: user });
        this._navigate('home', SlideLeft)
      }
      // console.warn(err);
    });
    // _navigate
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentDidMount() {
    this.callz = fireScratch
    .database()
    .ref('/callz');
    this.callz.on('child_added', (single_user) => {
      var currUser = single_user.val();
      if (currUser.recpt === this.state.email && currUser.status == 0) {
        callInfoPin = currUser;
        Transition.show(
          <Call
          nav={this._navigate}
          callInfo={currUser}
          dbHandle={fireScratch}
          transit={Transition}
          _database={AsyncStorage}>
          </Call>, FlipX);
        }
      });
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
      var page = this.state.previousPage;
      var pageCurr = this.state.currentPage;
      if (pageCurr === 'home' || pageCurr === 'intro') {
        Alert.alert(
          'Exit App',
          'Do you want to exit?', [{
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel'
          }, {
            text: 'OK',
            onPress: () => BackHandler.exitApp()
          }, ], {
            cancelable: false
          }
        )
      } else {

        this.gotoPage(page);
        this._navigate(page, SlideRight);
      }

      return true;
    }

    gotoPage = (page) => {
      this.setState({previousPage: this.state.currentPage})
      this.setState({currentPage: page})
    }

    _navigate = (page, transition) => {
      switch (page) {
        case 'login':
        Transition.show(
          <Login
          nav={this._navigate}
          dbHandle={fireScratch}
          _database={AsyncStorage}></Login>, transition);
          break;
          case 'signup':
          Transition.show(
            <Register nav={this._navigate} dbHandle={fireScratch} _database={AsyncStorage}></Register>, transition);
            break;
            case 'language':
            Transition.show(
              <Language nav={this._navigate} dbHandle={fireScratch} _database={AsyncStorage}></Language>, transition);
              break;
              case 'electronic':
              this.state.previousPage = 'home';
              this.state.currentPage = 'electronic';
              Transition.show(
                <Electronic
                nav={this._navigate}
                dbHandle={fireScratch}
                _database={AsyncStorage}></Electronic>, transition);
                break;
                case 'hooklive':
                this.state.previousPage = 'home';
                this.state.currentPage = 'hooklive';
                Transition.show(
                  <Hooklive
                  nav={this._navigate}
                  paid={PaymentStatus}
                  dbHandle={fireScratch}
                  paymentMail={userPaymentMail}
                  _database={AsyncStorage}></Hooklive>, transition);
                  break;
                  case 'live':
                  const container = this;
                  AsyncStorage
                  .getItem('CALL0010', (err, user) => {
                    if (err) {
                      Alert.alert('Error!')
                    } else {
                      // container.setState({recpt: user});
                      AsyncStorage
                      .getItem('UIID001', (err, me) => {
                        if (err) {
                          Alert.alert('Error!')
                        } else {
                          // container.setState({caller: me});
                          var call_room = {
                            caller: me,
                            recpt: user,
                            status: 0
                          }
                          console.warn(call_room);
                          let roomKey = Base64.encode(call_room.recpt);
                          // container.setState({roomID: roomKey});
                          // container.setState({status: 'connect', info: 'Connecting'});
                          // join(this.state.roomID);
                          fireScratch
                          .database()
                          .ref('/callz')
                          .child(roomKey)
                          .set(call_room);
                          // console.warn(container._navigate)
                          // console.warn(AsyncStorage)
                          // console.warn(call_room)
                          Transition.show(
                            <Live nav={container._navigate} dbHandle={fireScratch} _database={AsyncStorage} callInfo={call_room}></Live>, transition);
                          }
                          // console.warn(err);
                        });
                        // Alert.alert(user)
                      }
                      // console.warn(err);
                    });
                    break;
                    case 'liveIn':
                    Transition.show(
                      <Live nav={this._navigate} dbHandle={fireScratch} _database={AsyncStorage} callInfo={callInfoPin}></Live>
                      , transition);
                      break;
                      case 'home':
                      this.state.previousPage = 'home';
                      this.state.currentPage = 'home';
                      Transition.show(
                        <Home nav={this._navigate} dbHandle={fireScratch} _database={AsyncStorage}></Home>, transition);
                        break;
                        case 'intro':
                        Transition.show(
                          <Main nav={this._navigate} dbHandle={fireScratch} _database={AsyncStorage}></Main>, transition);
                          break;
                          default:
                          break;
                        }
                      };

                      render() {
                        const resizeMode = 'cover';
                        return (
                          <Transition
                          duration={300}
                          onTransitioned={(item) => console.log('Complete', item)}>
                          <Main nav={this._navigate}></Main>
                          </Transition>
                        )
                      }
                    }

                    const styles = {
                      wrapper: {
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#fff',
                        flex: 1,
                        padding: 0
                      },
                      box: {
                        padding: 10
                      },
                      subtitle: {
                        fontSize: 15,
                        fontWeight: '200',
                        color: '#fefefe'
                      },
                      hint: {
                        fontSize: 12,
                        fontWeight: '200',
                        color: '#fefefe'
                      },
                      signup: {
                        backgroundColor: '#5FA9DD',
                        borderColor: 'rgba(0,0,0,0)',
                        padding: 3
                      },
                      login_holder: {
                        paddingLeft: 20,
                        paddingRight: 20
                      },
                      signup_text: {
                        color: '#ffffff',
                        // fontWeight: '',
                        fontSize: 16
                      }
                    };