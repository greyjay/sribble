import React, {Component} from 'react';
import {StyleSheet, Alert, BackHandler, Image as Img, ScrollView, StatusBar} from 'react-native'
import {
  View,
  Text,
  NavigationBar,
  Icon,
  Title,
  Image,
  Button,
  Caption,
  TextInput
} from '@shoutem/ui';

import {
  Fade,
  FlipX,
  FlipY,
  SlideLeft,
  SlideRight,
  SlideUp,
  SlideDown
} from 'react-native-transition';

import Login from './login';
import Register from './register';

export default class main extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const resizeMode = 'cover'

    return (
      <ScrollView
        style={{
        flex: 1
      }}
        contentContainerStyle={styles.wrapper}>
          <StatusBar
            backgroundColor="#0c3959"
            barStyle="light-content"
        />
        <Image styleName="fill-parent" style={{}} source={require('./assets/bg.png')}/>
        <View styleName="fill-parent vertical h-center" Style={styles.wrapper}>
          <Image styleName="fill-parent" style={{}} source={require('./assets/bg.png')}/>
          <View
            styleName="flexible h-center v-start"
            style={{
            flex: 0.5
          }}></View>
          <View
            styleName="vertical flexible h-center v-start"
            style={{
            flex: 5
          }}>
            <Image styleName="small-avatar" source={require('./assets/icon.png')}  style={styles.logo_main}/>
            <Text styleName="bright" style={styles.subtitle}>The Instant Language Translator App</Text>
            <View
              styleName="flexible h-center v-start"
              style={{
              flex: 0.2
            }}></View>
            <Caption style={{
              color: '#ffffff'
            }}>No Account?</Caption>
            <View
              styleName="flexible h-center v-start"
              style={{
              flex: 0.03
            }}></View>
            <View styleName="horizontal" style={styles.box}>
              <Button
                styleName="confirmation"
                style={styles.signup}
                onPress={() => {
                this
                  .props
                  .nav('signup', SlideLeft);
              }}>
                <Text style={styles.signup_text}>Create Account</Text>
              </Button>
            </View>
            <View
              styleName="flexible h-center v-start"
              style={{
              flex: 0.09
            }}></View>
            <Caption style={{
              color: '#ffffff'
            }}>Already a user?</Caption>
            <View
              styleName="flexible h-center v-start"
              style={{
              flex: 0.03
            }}></View>
            <View styleName="horizontal" style={styles.login_holder}>
              <Button
                styleName="confirmation"
                onPress={() => {
                this
                  .props
                  .nav('login', SlideLeft);
              }}>
                <Text>Sign In</Text>
              </Button>
            </View>
          </View>
          <View
            styleName="flexible h-center v-start"
            style={{
            flex: 0.2
          }}></View>
        </View>
      </ScrollView>
    )
  }
}

const styles = {
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    flex: 1,
    padding: 0
  },
  box: {
    padding: 10
  },
  subtitle: {
    marginTop: 15,
    fontSize: 15,
    fontWeight: '200',
    color: '#5FA9DD',
    marginBottom: 15
  },
  hint: {
    fontSize: 12,
    fontWeight: '200',
    color: '#fefefe'
  },
  signup: {
    backgroundColor: '#5FA9DD',
    borderColor: 'rgba(0,0,0,0)',
    padding: 2
  },
  login_holder: {
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 10
  },
  signup_text: {
    color: '#ffffff',
    // fontWeight: '',
    fontSize: 16
  },
  logo_main: {
    marginTop: 20,
    height: 60,
    width: 200
  }
};