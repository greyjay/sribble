import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Alert,
  StatusBar,
  TouchableOpacity,
  AsyncStorage,
  Share,
  ToastAndroid
} from 'react-native'
import CardView from 'react-native-cardview';
import FontAwesome, {Icons} from 'react-native-fontawesome';
import Modal from "react-native-modal";

import {
  createTransition,
  Fade,
  FlipX,
  FlipY,
  SlideLeft,
  SlideRight,
  SlideUp,
  SlideDown
} from 'react-native-transition';

function mapHash(hash, func) {
  const array = [];
  for (const key in hash) {
    const obj = hash[key];
    array.push(func(obj, key));
  }
  return array;
}

var toDate = (string) => {
  var temp = string.split('_');
  var D = new Date(temp[0] * 1);
  // console.warn(D)
  var yr = D.getFullYear();
  var mnt = D.getMonth() + 1;
  var day = D.getDate();
  return day + '-' + mnt + '-' + yr;
}
var previewText = (string) => {
  return string
    .substring(0, 40)
    .trim() + '...';
}

getCatStyle = (cat) => {
  switch (cat) {
    case 'personal':
      return styles.personal;
      break;
    case 'work':
      return styles.work;
      break;
    case 'life':
      return styles.life;
      break;
    case 'study':
      return styles.study;
      break;

    default:
      break;
  }
}
export default class scribbleList extends Component {
  constructor(props) {
    super(props);
    this.addNew = this.addNew;

    this.state = {
      pageHeader: 'My Scribbles',
      allNotes: {},
      isModalVisible: false,
      selectedNote: null,
      selectedNoteKey: null
    }
    this._toggleModal = this._toggleModal;
    this.shareAction = this.shareAction;
    this.readAction = this.readAction;
    this.editAction = this.editAction;
    this.deleteAction = this.deleteAction;
    // AsyncStorage.setItem("grEyScriBble", '{}');
  }

  addNew = () => {
    this
      .props
      .nav('newscribble', SlideUp);
    // Alert.alert('Tyo')
  }

  _toggleModal = (note, key) => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
      selectedNote: note,
      selectedNoteKey: key
    });
  }

  readAction = () => {
    var extra = {
      key: this.state.selectedNoteKey,
      note: this.state.selectedNote
    }
    this.props.nav('readscribble', SlideLeft, extra);
  }

  editAction = () => {
    var extra = {
      key: this.state.selectedNoteKey,
      note: this.state.selectedNote
    }
    this.props.nav('editscribble', SlideLeft, extra);
  }

  deleteAction = () => {
    var key = this.state.selectedNoteKey;
    var temp = this.state.allNotes;
    delete temp[key];
    AsyncStorage.setItem("grEyScriBble", JSON.stringify(temp));
    this.setState({
      'allNotes': temp,
      isModalVisible: !this.state.isModalVisible
    });
    ToastAndroid.show('scRibble Deleted!', 800);
  }

  shareAction = () => {
    console.warn(this.state.selectedNote);
    console.warn(this.state.selectedNoteKey);
    Share.share({
      message: this.state.selectedNote.note,
      title: 'scRibble Share!!',
      url: 'http://schoolcloudng.herokuapp.com'
    }, {
      dialogTitle: 'Share scRibble!',
      tintColor: '#017afe'
    }).then((result) => {
      this.setState({
        isModalVisible: !this.state.isModalVisible
      });
      ToastAndroid.show("scRibble Shared!", 800);
    }).catch(err => console.log(err))
  }

  componentDidMount() {
    AsyncStorage
      .getItem("grEyScriBble")
      .then((value) => {
        this.setState({
          "allNotes": JSON.parse(value)
        });
        // console.warn(JSON.parse(value));
      })
      .done();
  }

  render() {
    const container = this;
    const resizeMode = 'cover'
    return (
      <View style={styles.wrapper}>
        <StatusBar backgroundColor="#fff" barStyle="light-content"/>
        <View style={styles.headerBox}>
          <Image style={styles.topImg} source={require('./assets/top.png')}/>
          <Text style={styles.header}>
            {this.state.pageHeader}</Text>
        </View>

        <ScrollView
          style={{
          flex: 1,
          backgroundColor: '#fff'
        }}
          contentContainerStyle={styles.contentWrapper}>
          <View
            style={{
            flex: 1,
            flexGrow: 1,
            flexShrink: 1,
            flexBasis: 1
          }}>
            {(this.state.allNotes !== null && this.state.allNotes !== {}) && mapHash(this.state.allNotes, function (singleNote, index) {
              return (
                <TouchableOpacity onPress={(e) => container._toggleModal(singleNote, index, e)}>
                  <CardView
                    cardElevation={3}
                    style={styles.singleListItem}
                    cardMaxElevation={3}
                    cornerRadius={6}>
                    <View style={styles.cardHeader}>
                      <Text style={styles.listDate}>{toDate(index)}</Text>
                      <Text
                        style={[
                        styles.listText,
                        getCatStyle(singleNote.category)
                      ]}>{singleNote
                          .category
                          .charAt(0)
                          .toUpperCase() + singleNote
                          .category
                          .slice(1)}</Text>
                    </View>
                    <View style={styles.cardContent}>
                      <Text style={styles.cardText}>{previewText(singleNote.note)}</Text>
                    </View>
                  </CardView>
                </TouchableOpacity>
              )
            })
}
            {(this.state.allNotes === null || this.state.allNotes === {}) && <CardView
              cardElevation={0}
              style={styles.emptyShii}
              cardMaxElevation={0}
              cornerRadius={6}>
              <FontAwesome style={styles.empIcon}>{Icons.mehO}</FontAwesome>
              <Text style={styles.empHeader}>No scriBble yet</Text>
            </CardView>
}
          </View>
        </ScrollView>

        <View style={styles.actionBar}>

          <TouchableOpacity style={styles.addNote} onPress={this.addNew}>
            <View >
              <Text style={styles.addNoteText}>+</Text>
            </View>
          </TouchableOpacity>

          <Modal
            isVisible={this.state.isModalVisible}
            onBackdropPress={() => this.setState({isModalVisible: false})}>
            <View
              style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
              <TouchableOpacity style={styles.closeHanger} onPress={this._toggleModal}>
                <FontAwesome
                  style={{
                  color: '#95a5a6',
                  fontSize: 20
                }}>{Icons.times}</FontAwesome>
              </TouchableOpacity>
              <CardView
                cardElevation={3}
                style={styles.popUpOptions}
                cardMaxElevation={3}
                cornerRadius={6}>
                <View style={styles.modalContent}>

                  <View style={styles.menuRow}>
                    <View style={styles.singleMenu}>
                      <TouchableOpacity style={styles.secondPin} onPress={(e) => container.editAction(e)}>
                        <View>
                          <FontAwesome style={[styles.defaultBtn]}>{Icons.pencil}</FontAwesome>
                        </View>
                      </TouchableOpacity>
                      <Text>Edit</Text>
                    </View>
                    <View style={styles.singleMenu}>
                      <TouchableOpacity style={styles.secondPin} onPress={(e) => container.readAction(e)}>
                        <View>
                          <FontAwesome style={[styles.defaultBtn]}>{Icons.fileO}</FontAwesome>
                        </View>
                      </TouchableOpacity>
                      <Text>Read</Text>
                    </View>
                  </View>

                  <View style={styles.menuRow}>
                    <View style={styles.singleMenu}>
                      <TouchableOpacity style={styles.secondPin} onPress={(e) => container.deleteAction(e)}>
                        <View>
                          <FontAwesome style={[styles.defaultBtn]}>{Icons.trashO}</FontAwesome>
                        </View>
                      </TouchableOpacity>
                      <Text>Delete</Text>
                    </View>
                    <View style={styles.singleMenu}>
                      <TouchableOpacity
                        style={styles.secondPin}
                        onPress={(e) => container.shareAction(e)}>
                        <View>
                          <FontAwesome style={[styles.defaultBtn]}>{Icons.shareAlt}</FontAwesome>
                        </View>
                      </TouchableOpacity>
                      <Text>Share</Text>
                    </View>
                  </View>
                </View>
              </CardView>
            </View>
          </Modal>

        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    // justifyContent: 'center', alignItems: 'center',

    height: '100%',
    width: '100%',
    flex: 1,
    padding: 0,
    flexDirection: 'column'
  },
  empIcon: {
    fontSize: 50,
    fontWeight: "200",
    color: 'rgba(0,0,0,0.2)',
    transform: [
      {
        rotate: '30deg'
      }
    ]
  },
  empHeader: {
    fontSize: 30,
    fontFamily: "raleway",
    marginTop: 6,
    fontWeight: "400",
    color: 'rgba(0,0,0,0.2)'
  },
  header: {
    fontSize: 30,
    fontFamily: "raleway",
    padding: 20,
    marginTop: 15,
    fontWeight: "400",
    color: 'rgba(0,0,0,0.5)'
  },
  contentWrapper: {
    backgroundColor: '#cecece',
    padding: 16,
    minHeight: '75%',
    // flex: 1
  },
  singleListItem: {
    padding: 10,
    backgroundColor: '#ffffff',
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowColor: '#000',
    minHeight: 110,
    shadowOffset: {
      height: 2,
      width: 0
    },
    flexDirection: 'column'
  },
  popUpOptions: {
    padding: 8,
    paddingTop: 10,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
    width: '70%',
    height: 180
  },
  emptyShii: {
    flex: 1,
    height: 400,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#cecece'
  },
  cardHeader: {
    flex: 1,
    maxHeight: 20,
    flexDirection: 'row',
    paddingTop: 3,
    paddingRight: 15
  },
  cardContent: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 15,
    paddingRight: 30
  },
  modalContent: {
    flex: 1,
    flexDirection: 'column',
    // padding: 5
  },
  cardHeader: {
    flex: 1,
    maxHeight: 20,
    flexDirection: 'row',
    paddingTop: 3,
    paddingRight: 15
  },
  listDate: {
    flex: 1,
    fontSize: 13,
    fontFamily: "raleway",
    color: '#424242'
  },
  listText: {
    fontSize: 10,
    textAlign: 'right',
    fontFamily: "raleway",
    color: '#fff',
    backgroundColor: '#017afe',
    borderRadius: 8,
    paddingRight: 8,
    paddingLeft: 8,
    paddingTop: 3
  },
  work: {
    backgroundColor: '#ff9f43'
  },
  life: {
    backgroundColor: '#017afe'
  },
  study: {
    backgroundColor: '#27ae60'
  },
  personal: {
    backgroundColor: '#fd6b82'
  },
  cardText: {
    flex: 1,
    fontSize: 15,
    fontFamily: "raleway",
    writingDirection: 'ltr'
  },
  actionBar: {
    height: 50,
    // backgroundColor: '#ff5896',
    justifyContent: 'center',
    alignItems: 'center'
  },
  addNote: {
    backgroundColor: '#3e9bfd',
    height: 60,
    width: 60,
    borderRadius: 50,
    marginTop: -30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  addNoteText: {
    fontSize: 30,
    color: '#fff',
    fontFamily: "raleway"
  },
  headerBox: {},
  topImg: {
    position: 'absolute',
    marginTop: -20,
    marginLeft: -20,
    // height: 0,
  },
  invisible: {
    height: 400
  },
  secondPin: {
    backgroundColor: '#fd6b82',
    height: 50,
    width: 50,
    borderRadius: 50,
    marginTop: -20,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    marginRight: 5,
    flexDirection: 'column'
  },
  menuRow: {
    // marginTop: 10,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    // backgroundColor: "green",
    flex: 1
  },
  singleMenu: {
    // marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    marginRight: 5,
    flexDirection: 'column',
    flex: 1
  },
  defaultBtn: {
    color: '#fff',
    fontSize: 20
  },
  closeHanger: {
    backgroundColor: '#fff',
    height: 40,
    width: 40,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: -12
  }
});