import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Alert,
  StatusBar,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  ToastAndroid
} from 'react-native'
import CardView from 'react-native-cardview';
import FontAwesome, {Icons} from 'react-native-fontawesome';

import {
  createTransition,
  Fade,
  FlipX,
  FlipY,
  SlideLeft,
  SlideRight,
  SlideUp,
  SlideDown
} from 'react-native-transition';

function generateKey() {
  return Date.now() + '_' + Math
    .random()
    .toString(36)
    .substring(2, 15) + Math
    .random()
    .toString(36)
    .substring(2, 15);
}

const container = null;
export default class readScribble extends Component {
  constructor(props) {
    super(props);
    this.setCat = this
      .setCat
      .bind(this);
    this.state = {
      pageHeader: `Read Scribble (${this.props.extra.note.category})`,
      myNote: this.props.extra.note.note,
      allNotes: {},
      activeCat: 'personal'
    }
    console.warn(this.props.extra);
  }

  componentDidMount() {
    container = this;
    // console.warn(this.state.noteKey);
  }

  setCat(cat) {
    var tempCat = cat
      .charAt(0)
      .toUpperCase() + cat.slice(1);
    this.setState({'activeCat': cat});
    ToastAndroid.show(tempCat, 800);
  }

  render() {
    const resizeMode = 'cover'
    return (
      <View style={styles.wrapper}>
        <StatusBar backgroundColor="#fff" barStyle="light-content"/>
        <View style={styles.headerBox}>
          <Image style={styles.topImg} source={require('./assets/top.png')} />
          <Text style={styles.header}>{this.state.pageHeader}</Text>
        </View>
        <TouchableOpacity style={styles.backButton} onPress={
          () => {
            this.props.nav('listscribble', SlideLeft);
          }
        }>
            <FontAwesome style={[styles.defaultBtn, styles.backIcon]}>{Icons.chevronLeft}</FontAwesome>
          </TouchableOpacity>
        <ScrollView
          style={{
            flex: 1,
            backgroundColor: '#e5ddd5'
          }}
          contentContainerStyle={styles.contentWrapper}>
          <TextInput
            style={styles.input}
            underlineColorAndroid="transparent"
            placeholder="Wanna Write?"
            placeholderTextColor="rgba(0,0,0,0.3)"
            multiline={true}
            editable={false}
            value={this.state.myNote}/>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    // justifyContent: 'center', alignItems: 'center',
    height: '100%',
    width: '100%',
    flex: 1,
    padding: 0,
    flexDirection: 'column'
  },
  header: {
    fontSize: 20,
    fontFamily: "raleway",
    padding: 20,
    paddingLeft: 50,
    fontWeight: "100",
    color: 'rgba(0,0,0,0.5)',
    marginTop: 15
  },
  contentWrapper: {
    backgroundColor: '#cecece',
    padding: 14,
    paddingTop: 0,
    paddingBottom: 0,
    height: '100%'
    // flex: 1
  },
  singleListItem: {
    padding: 10,
    backgroundColor: '#ffffff',
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowColor: '#000',
    minHeight: 110,
    shadowOffset: {
      height: 2,
      width: 0
    },
    flexDirection: 'column'
  },
  cardHeader: {
    flex: 1,
    maxHeight: 20,
    flexDirection: 'row',
    paddingTop: 3,
    paddingRight: 15
  },
  cardContent: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 15,
    paddingRight: 30
  },
  cardHeader: {
    flex: 1,
    maxHeight: 20,
    flexDirection: 'row',
    paddingTop: 3,
    paddingRight: 15
  },
  listDate: {
    flex: 1,
    fontSize: 13,
    fontFamily: "raleway",
    color: '#424242'
  },
  listText: {
    fontSize: 10,
    textAlign: 'right',
    fontFamily: "raleway",
    color: '#fff',
    backgroundColor: '#017afe',
    borderRadius: 8,
    paddingRight: 8,
    paddingLeft: 8,
    paddingTop: 3
  },
  work: {
    backgroundColor: '#ff5896'
  },
  cardText: {
    flex: 1,
    fontSize: 15,
    fontFamily: "raleway",
    writingDirection: 'ltr'
  },
  actionBar: {
    height: 50,
    // backgroundColor: '#ff5896',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  addNote: {
    backgroundColor: '#3e9bfd',
    height: 60,
    width: 60,
    borderRadius: 50,
    marginTop: -30,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    marginRight: 5
  },
  addNoteText: {
    fontSize: 15,
    color: '#fff',
    fontFamily: "raleway"
  },
  input: {
    fontFamily: "raleway",
    justifyContent: 'flex-start',
    textAlign: 'left',
    lineHeight: 20,
    fontSize: 15,
    // height: '100%',
    color: '#424242'
  },
  headerBox: {
  },
  topImg: {
    position: 'absolute',
    marginTop: -20,
    marginLeft: -20,
    // height: 0,
  },
  secondPin: {
    backgroundColor: '#fd6b82',
    height: 50,
    width: 50,
    borderRadius: 50,
    marginTop: -20,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    marginRight: 5,
    flexDirection: 'column'
  },
  firstPin: {
    backgroundColor: '#ff9f43',
    height: 40,
    width: 40,
    borderRadius: 50,
    marginTop: -10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    marginRight: 5,
    flexDirection: 'column'
  },
  defaultBtn: {
    color: '#fff'
  },
  workActive: {
    color: '#fd6b82'
  },
  lifeActive: {
    color: '#ff9f43'
  },
  selected: {
    width: 5,
    height: 5,
    borderRadius: 50,
    backgroundColor: '#ffffff',
    marginBottom: 0,
    marginTop: 4,
    marginLeft: 5
  },
  backIcon: {
    fontSize: 20,
    color: 'rgba(0,0,0,0.5)',
  },
  backButton: {
    fontSize: 15,
    position: 'absolute',
    paddingLeft: 25,
    paddingTop: 37
  }
});