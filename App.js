/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/
import SplashScreen from 'react-native-splash-screen'
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  BackHandler,
  Alert
} from 'react-native';

import {
  createTransition,
  Fade,
  FlipX,
  FlipY,
  SlideLeft,
  SlideRight,
  SlideUp,
  SlideDown
} from 'react-native-transition';

// Pages

import ScribbleList from './pages/scribbleList';
import NewScribble from './pages/newScribble';
import ReadScribble from './pages/readScribble';
import EditScribble from './pages/editScribble';

Transition = createTransition();

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
  'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
  'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this
    .handleBackButtonClick
    .bind(this);
    this.gotoPage = this.gotoPage;
    this.state = {
      previousPage: 'intro',
      currentPage: 'intro',
      loggedIn: false,
      email: ''
    };
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
    SplashScreen.hide();
    this._navigate('listscribble', FlipY);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    var page = this.state.previousPage;
    var pageCurr = this.state.currentPage;
    if (pageCurr === 'listscribble' || pageCurr === 'intro') {
      Alert.alert(
        'Exit App',
        'Do you want to exit?', [{
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        }, {
          text: 'OK',
          onPress: () => BackHandler.exitApp()
        }, ], {
          cancelable: false
        }
      )
    } else {
      this.gotoPage(page);
      this._navigate(page, SlideRight);
    }
    return true;
  }

  gotoPage = (page) => {
    this.setState({previousPage: this.state.currentPage})
    this.setState({currentPage: page})
  }

  _navigate = (page, transition, extra=null) => {
    switch (page) {
      case 'listscribble':
      Transition.show(
        <ScribbleList
        nav={this._navigate}></ScribbleList>, transition);
        break;
      case 'newscribble':
      Transition.show(
        <NewScribble
        nav={this._navigate}></NewScribble>, transition);
        break;
      case 'readscribble':
      Transition.show(
        <ReadScribble
          nav={this._navigate} extra={extra}></ReadScribble>, transition);
        break;
      case 'editscribble':
      Transition.show(
        <EditScribble
          nav={this._navigate} extra={extra}></EditScribble>, transition);
        break;
        default:
        break;
    }
    this.gotoPage(page)
    };

    render() {
      return (
        <Transition
        duration={300}
        onTransitioned={(item) => console.log('Complete', item)}>
        {/* <Main nav={this._navigate}></Main> */}
        <View style={styles.container}>
        <Text style={styles.welcome}>
        Welcome to React Native!
        </Text>
        <Text style={styles.instructions}>
        To get started, edit App.js
        </Text>
        <Text style={styles.instructions}>
        {instructions}
        </Text>
        </View>
        </Transition>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
      fontFamily: "raleway"
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });
