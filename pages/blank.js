import React, {Component} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native'

export default class login extends Component {
  render() {
    const resizeMode = 'cover'
    return (
      <View style={styles.wrapper}>
        <Image
          style={{
            flex: 1,
            resizeMode,
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          }}
          source={require('./assets/bg.png')}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    flex: 1,
    padding: 0
  },
});